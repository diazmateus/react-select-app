import React from 'react';
import SelectSearch from 'react-select-search';
import './select.css'

const options = [
    {name: 'Swedish', value: 'sv'},
    {name: 'English', value: 's5d4f'},
    {name: 'English', value: 'ens5df52'},
    {name: 'English', value: 'enfsd5'},
    {name: 'English', value: 'ensfd'},
    {name: 'English', value: 'ensdf54'},
    {name: 'English', value: 'ensdf4'},
    {name: 'English', value: 'ensdf44'},
];

class Select extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            selected: ''
        }
    }

    onChange = (value) => {
        this.setState({
            selected: value
        })
    };

    render() {
        const {selected} = this.state;
        return (
            <SelectSearch
                options={options}
                value={selected}
                onChange={it => this.onChange(it.value)}
                name="select"
                placeholder="Selecione uma cidade"/>
        );
    }
}

export default Select;